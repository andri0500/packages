# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Maintainer: Adélie Perl Team <adelie-perl@lists.adelielinux.org>
pkgname=perl-digest-sha1
_pkgreal=Digest-SHA1
_author=GAAS
_au=${_author%%"${_author#??}"}
_a=${_author%%"${_author#?}"}
pkgver=2.13
pkgrel=11
pkgdesc="Perl interface to the SHA-1 algorithm"
url="https://metacpan.org/release/Digest-SHA1"
arch="all"
license="GPL-2.0-only OR Artistic-1.0-Perl"
depends="perl"
makedepends="perl-dev"
subpackages="$pkgname-doc"
source="https://cpan.metacpan.org/authors/id/$_a/$_au/$_author/$_pkgreal-$pkgver.tar.gz
	perl-digest-sha1-check-object.patch
	"
builddir="$srcdir/$_pkgreal-$pkgver"

build() {
	PERL_MM_USE_DEFAULT=1 perl Makefile.PL INSTALLDIRS=vendor \
		OPTIMIZE="$CFLAGS"
	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install
	# creates file collision among perl modules
	find "$pkgdir" -name perllocal.pod -delete
}

sha512sums="44d0c57ecc7d2126a0387552e76c9204e45fba174af6ff7abc1c9ae00d549eb7370ee20948caf12fafefedec0098b8231249d14b109c53470ee1d5bf3de3305d  Digest-SHA1-2.13.tar.gz
73547d04bbd77cb82f0611132c2105574f528f2a07f4de436c41af606ec505a6a4b634f4397f4cee2d9aa94687957515ac8546b264ca8f71cbd4d4f5fdd5ee74  perl-digest-sha1-check-object.patch"

# Maintainer: Max Rees <maxcrees@me.com>
pkgname=mandoc
pkgver=1.14.6
pkgrel=0
pkgdesc="Manual page database, compiler, and viewer"
url="https://mandoc.bsd.lv/"
arch="all"
license="ISC AND BSD-2-Clause AND BSD-3-Clause"
replaces="man-db"
depends="less"
makedepends="zlib-dev"
checkdepends="perl"
subpackages="$pkgname-doc $pkgname-cgi"
install="$pkgname.post-deinstall"
triggers="$pkgname.trigger=/usr/share/man"
source="https://mandoc.bsd.lv/snapshots/mandoc-$pkgver.tar.gz
	configure.patch
	less.patch
	doc.patch
	cgi-adelie.patch
	cgi-gz.patch
	cgi-search.patch
	"

prepare() {
	default_prepare
	cat >"configure.local" <<-EOF
		PREFIX=/usr
		MANDIR=/usr/share/man
		WWWPREFIX=/var/lib/mandoc-cgi
		# remove /usr/X11R6 from the following.
		MANPATH_BASE=/usr/share/man
		MANPATH_DEFAULT=/usr/share/man:/usr/local/share/man
		# groff also provides a soelim.
		BINM_SOELIM=mandoc-soelim

		CFLAGS="$CFLAGS"
		LDFLAGS="$LDFLAGS"
		LN="ln -sf"
		OSNAME="Adelie Linux"
		UTF8_LOCALE=C.UTF-8

		BUILD_CATMAN=1
		BUILD_CGI=1
	EOF

	cat >"cgi.h" <<-EOF
		#define SCRIPT_NAME ""
		#define MAN_DIR "/var/lib/mandoc-cgi"
		#define CSS_DIR ""
		#define CUSTOMIZE_TITLE "Adelie Linux manual pages"
	EOF
}

build() {
	./configure
	make
}

check() {
	make regress
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm644 -t "$pkgdir"/usr/share/man/man8 man.cgi.8

	mkdir -p "$pkgdir"/etc
	cat >"$pkgdir"/etc/man.conf <<-EOF
		# See man.conf(5).
		#manpath /usr/share/man
		#manpath /usr/local/share/man
	EOF
}

cgi() {
	pkgdesc="$pkgdesc (web interface)"
	depends="$pkgname=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir"/var/lib
	mv "$pkgdir"/var/lib/mandoc-cgi \
		"$subpkgdir"/var/lib
}

sha512sums="54286070812a47b629f68757046d3c9a1bdd2b5d1c3b84a5c8e4cb92f1331afa745443f7238175835d8cfbe5b8dd442e00c75c3a5b5b8f8efd8d2ec8f636dad4  mandoc-1.14.6.tar.gz
08d11f7f0ae66e1630796d928c3906914f4133906928f74b93a1b08207484d16d412577ed739a920cd87abcec720b69fe121da1338a59d2580b978feadeedf32  configure.patch
dac7b07cff35899b004a334e2926662647da274f7a946ccbc5131e8dd81e32e14073dc93110410baa183da1ffcc53b653c415d89071a1bd941dbf0e88091dee0  less.patch
c7e3afdf9c9a3c51237e860841902074e17e143433f314961b6f804b7b3a0f176855648180dd9a836848b1c54506344b09dab3577dea58e2ddf5bf5915196419  doc.patch
098af21312c4acdec33b5817a60f3ed5e91685fc225ec051ad5326d8df3f5ab52a4725d30d52a3fef580de7e7d7ff16e8e7352b9d387edbea56dd816b04e3f19  cgi-adelie.patch
957bbaef888fb2ea15f9b795b15952332e65aed70d2e70f3142305a4fb9313ade455eae6dfc923af3ef008834e771b1a85d71389b86cd0ff9a1c31e84fef861c  cgi-gz.patch
be7d734ceb436764ad9bd23ac8c4c4b486ad88521e54302524aeb3fc00b4f27939a19495c920ed1be1f82c58fcc72bbb930757a2ac5ad99532929b3e6b3d7d77  cgi-search.patch"

# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=firefox
pkgver=68.0.1
pkgrel=0
pkgdesc="Firefox Web browser (unstable)"
url="https://www.mozilla.org/firefox/"
arch="all"
options="!check"  # Tests disabled
license="MPL-2.0"
depends=""
# moz build system stuff
# python deps
# system-libs
# actual deps
makedepends="
	autoconf2.13 cargo cbindgen clang llvm8-dev node ncurses-dev 
	perl rust rust-stdlib cmd:which

	ncurses-dev openssl-dev

	alsa-lib-dev bzip2-dev icu-dev libevent-dev libffi-dev libpng-dev
	libjpeg-turbo-dev nspr-dev nss-dev pulseaudio-dev zlib-dev

	dbus-glib-dev fts-dev gconf-dev gtk+3.0-dev hunspell-dev
	libnotify-dev libsm-dev libxcomposite-dev libxdamage-dev
	libxrender-dev libxt-dev nasm nss-static sqlite-dev
	startup-notification-dev unzip yasm zip gtk+2.0-dev
	"
_py2ver="2.7.16"
source="https://ftp.mozilla.org/pub/firefox/releases/$pkgver/source/firefox-$pkgver.source.tar.xz
	https://www.python.org/ftp/python/$_py2ver/Python-$_py2ver.tar.xz
	mozconfig

	bad-google-code.patch
	disable-gecko-profiler.patch
	fix-mutex-build.patch
	fix-seccomp-bpf.patch
	mozilla-build-arm.patch
	rust-config.patch
	shut-up-warning.patch
	stackwalk-x86-ppc.patch
	webrtc-broken.patch
	yuv-be.patch

	firefox.desktop
	firefox-safe.desktop
	"

_mozappdir=/usr/lib/firefox
ldpath="$_mozappdir"

unpack() {
	default_unpack
	[ -z $SKIP_PYTHON ] || return 0

	msg "Killing all remaining hope for humanity and building Python 2..."
	cd "$srcdir/Python-$_py2ver"
	[ -d ../python ] && rm -r ../python

	# 19:39 <+solar> just make the firefox build process build its own py2 copy
	# 20:03 <calvin> TheWilfox: there's always violence

	sed -e 's/é/e/g' /etc/os-release > "$srcdir"/os-release
	export UNIXCONFDIR="$srcdir"

	./configure --prefix="$srcdir/python" --with-ensurepip=install
	make -j $JOBS
	# 6 tests failed:
	#    test__locale test_os test_posix test_re test_strptime test_time
	# make test
	make -j $JOBS install

	# firefox's bundled pipenv and pip aren't new enough to support
	# configurable UNIXCONFDIR
	export PATH="$srcdir/python/bin:$PATH"
	pip2 install virtualenv pipenv
}

prepare() {
	cd "$builddir"
	default_prepare
	cp "$srcdir"/mozconfig "$builddir"/mozconfig
	echo "ac_add_options --enable-optimize=\"$CFLAGS\"" >> "$builddir"/mozconfig
	echo "ac_add_options --host=\"$CHOST\"" >> "$builddir"/mozconfig
	echo "ac_add_options --target=\"$CTARGET\"" >> "$builddir"/mozconfig
	# too much memory
	if [ -z "$JOBS" ] || [ $JOBS -ge 16 ]; then
		JOBS=16
	fi
	echo "mk_add_options MOZ_MAKE_FLAGS=\"-j$JOBS\"" >> "$builddir"/mozconfig

	case "$CARCH" in
		pmmx|x86*)
			echo "ac_add_options --disable-elf-hack" >> "$builddir"/mozconfig
			;;
	esac

	rm "$builddir"/third_party/python/virtualenv/virtualenv_support/pip*.whl
	rm "$builddir"/third_party/python/virtualenv/virtualenv_support/setuptools*.whl
	cp "$srcdir/Python-$_py2ver"/Lib/ensurepip/_bundled/*.whl \
		"$builddir/third_party/python/virtualenv/virtualenv_support"
}

build() {
	cd "$builddir"

	export SHELL=/bin/sh
	export BUILD_OFFICIAL=1
	export MOZILLA_OFFICIAL=1
	export USE_SHORT_LIBNAME=1
	# gcc 6
	export CXXFLAGS="-fno-delete-null-pointer-checks -fno-schedule-insns2"

	# set rpath so linker finds the libs
	export LDFLAGS="$LDFLAGS -Wl,-rpath,${_mozappdir}"

	export UNIXCONFDIR="$srcdir"

	local extra_flags=""
	[ "$CARCH" = "s390x" ] && extra_flags="--disable-startupcache"

	export PATH="$srcdir/python/bin:$PATH"
	./mach build
}

run() {
	cd "$builddir"/obj-$CHOST/dist/bin
	export LD_LIBRARY_PATH=.
	./firefox -no-remote -profile "$builddir"/obj-$CHOST/tmp/profile-default
}

package() {
	cd "$builddir"
	export PATH="$srcdir/python/bin:$PATH"
	DESTDIR="$pkgdir" ./mach install

	install -m755 -d ${pkgdir}/usr/share/applications
	install -m755 -d ${pkgdir}/usr/share/pixmaps

	local png
	for png in browser/branding/official/default*.png; do
		local i="${_png%.png}"
		i=${i##*/default}
		install -D -m644 "$png" \
			"$pkgdir"/usr/share/icons/hicolor/${i}x${i}/apps/firefox.png
	done

	install -m644 "$builddir"/browser/branding/official/default48.png \
		${pkgdir}/usr/share/pixmaps/firefox.png
	install -m644 ${startdir}/firefox.desktop \
		${pkgdir}/usr/share/applications/firefox.desktop
	install -m644 ${startdir}/firefox-safe.desktop \
		${pkgdir}/usr/share/applications/firefox-safe.desktop

	# install our vendor prefs
	install -d "$pkgdir"/$_mozappdir/browser/defaults/preferences

	cat >> "$pkgdir"/$_mozappdir/browser/defaults/preferences/firefox-branding.js <<- EOF
	// Use LANG environment variable to choose locale
	pref("intl.locale.matchOS", true);

	// Disable default browser checking.
	pref("browser.shell.checkDefaultBrowser", false);

	// Don't disable our bundled extensions in the application directory
	pref("extensions.autoDisableScopes", 11);
	pref("extensions.shownSelectionUI", true);
	EOF
}

sha512sums="96b45135cf0b2368013afccb8c375de54d591a4e11016e8b65fc83904cedc362096dd15814cd02be23f6e52e392c605817b86a59ee2300d3e7a754d345399c81  firefox-68.0.1.source.tar.xz
16e814e8dcffc707b595ca2919bd2fa3db0d15794c63d977364652c4a5b92e90e72b8c9e1cc83b5020398bd90a1b397dbdd7cb931c49f1aa4af6ef95414b43e0  Python-2.7.16.tar.xz
8748e7541fd94c83bb87ff1840f6fd012c989c66af555679bf4a40b1b343a3d2191d324ad4ca1a28b450fbf6418299b89ca9617cd769b0e1512837860c8111aa  mozconfig
ace7492f4fb0523c7340fdc09c831906f74fddad93822aff367135538dacd3f56288b907f5a04f53f94c76e722ba0bab73e28d83ec12d3e672554712e6b08613  bad-google-code.patch
9c14041f0295682b8dbeb6d5b58a2f9dc0a2dc8bef995a0f7e30fa0b17c51aa0f6748f80fb8584169db7687e2eeb404dff68a09158ae56a5f24eef30685dd2b3  disable-gecko-profiler.patch
c0b2bf43206c2a5154e560ef30189a1062ae856861b39f52ce69002390ff9972d43e387bfd2bf8d2ab3cac621987bc042c8c0a8b4cf90ae05717ca7705271880  fix-mutex-build.patch
70863b985427b9653ce5e28d6064f078fb6d4ccf43dd1b68e72f97f44868fc0ce063161c39a4e77a0a1a207b7365d5dc7a7ca5e68c726825eba814f2b93e2f5d  fix-seccomp-bpf.patch
e61664bc93eadce5016a06a4d0684b34a05074f1815e88ef2613380d7b369c6fd305fb34f83b5eb18b9e3138273ea8ddcfdcb1084fdcaa922a1e5b30146a3b18  mozilla-build-arm.patch
45613d476e85fe333ef8091acce4806803953c1a99de4f03ff577cf20c5a1a3d635d0589e1490da104ef80721f4f1b1d35045af3c6892c1a468fa84095f27ad8  rust-config.patch
39ddb15d1453a8412275c36fc8db3befc69dffd4a362e932d280fb7fd1190db595a2af9b468ee49e0714f5e9df6e48eb5794122a64fa9f30d689de8693acbb15  shut-up-warning.patch
452b47b825294779f98ed46bc1065dad76b79ff453521ef049934a120f349c84a1c863b16af1828fe053059823da9690ec917c055ae02dcc5c80c54cad732448  stackwalk-x86-ppc.patch
be68f1387aa6677875a67106e2d6a9db470c934c943056d3b53391a63034235108e41945c53957db427d9cdc59f0aa2f9e6f2f8cd862e090e512a3ab9cbcc9a8  webrtc-broken.patch
2dfb986089c9afcd6a895302c8a5a1d299cffa4cc3c73fce784c29d348f362c1e7570109c4f09d328275d8549a96531736dd976411c15956b385d7fb211b8af2  yuv-be.patch
f3b7c3e804ce04731012a46cb9e9a6b0769e3772aef9c0a4a8c7520b030fdf6cd703d5e9ff49275f14b7d738fe82a0a4fde3bc3219dff7225d5db0e274987454  firefox.desktop
5dcb6288d0444a8a471d669bbaf61cdb1433663eff38b72ee5e980843f5fc07d0d60c91627a2c1159215d0ad77ae3f115dcc5fdfe87e64ca704b641aceaa44ed  firefox-safe.desktop"

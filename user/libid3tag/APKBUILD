# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: 
pkgname=libid3tag
pkgver=0.15.1b
pkgrel=10
pkgdesc="Library for manipulating IDv3 tags in MP3 audio files"
url="http://www.underbit.com/products/mad/"
arch="all"
license="GPL-2.0+"
depends=""
makedepends="gperf zlib-dev"
subpackages="$pkgname-dev"
source="ftp://ftp.mars.org/pub/mpeg/libid3tag-$pkgver.tar.gz
	CVE-2004-2779.patch
	CVE-2017-11550.patch
	gperf.patch
	"

# secfixes:
#   0.15.1b-r8:
#     - CVE-2008-2109
#   0.15.1b-r10:
#     - CVE-2004-2779
#     - CVE-2017-11550
#     - CVE-2017-11551

prepare() {
	update_config_sub
	default_prepare
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	mkdir -p "$pkgdir"/usr/lib/pkgconfig
	cat > "$pkgdir"/usr/lib/pkgconfig/id3tag.pc <<EOF
prefix=/usr
exec_prefix=/usr/bin
libdir=/usr/lib
includedir=/usr/include

Name: id3tag
Description: ID3 tag manipulation library
Requires: zlib
Version: $pkgver
Libs: -lid3tag
Cflags:
EOF
}

sha512sums="ade7ce2a43c3646b4c9fdc642095174b9d4938b078b205cd40906d525acd17e87ad76064054a961f391edcba6495441450af2f68be69f116549ca666b069e6d3  libid3tag-0.15.1b.tar.gz
4c27e104d45ae34affc1bef8ec613e65c7e4791185d2ef1cb27974ec7025c06c35d30d6278ce7e3107dff959bd55a708246c3c1a9d5ad7b093424cfb93b79f63  CVE-2004-2779.patch
6627d6e73958309b199a02cd6fa1008a81554151238d8a099dc27e535b8d14f7a9c1ba19894fdf2c927e59c0ca855d50b2f1289f116b45bc41e02d31659d1535  CVE-2017-11550.patch
3f320d1d8719afd84aa0cf2a9e5615f3f535f84d1987f12df0a9d3f2b7c02e2c87fbc3aa41d538cdc4f8a30e629de6f3fc3a4e79f23448fd39d4c0c438c803fb  gperf.patch"

# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kmime
pkgver=22.04.2
pkgrel=0
pkgdesc="KDE support library for MIME"
url="https://www.kde.org/"
arch="all"
license="BSD-3-Clause AND CC0-1.0 AND LGPL-2.0+ AND LGPL-2.0-only"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 kcodecs-dev
	ki18n-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmime-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# https://bugs.kde.org/show_bug.cgi?id=385479
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(headertest|messagetest|dateformattertest)'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="8f7f9f253566b511f4d8c4fe9f6148b5b4d82326ece70d9ee247b600e4226042ea92a6dbcd1636aea876f2794c5d335f899de723bf429e8a0732d8e1f06cbadc  kmime-22.04.2.tar.xz"

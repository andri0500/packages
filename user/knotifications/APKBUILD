# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=knotifications
pkgver=5.94.0
pkgrel=0
pkgdesc="Framework for sending notifications to users"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires DBus daemon running.
license="LGPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtx11extras-dev kwindowsystem-dev kconfig-dev
	kcodecs-dev kcoreaddons-dev phonon-dev libdbusmenu-qt-dev"
makedepends="$depends_dev cmake extra-cmake-modules libx11-dev libxext-dev
	libice-dev qt5-qtspeech-dev qt5-qttools-dev doxygen graphviz
	libcanberra-dev qt5-qtdeclarative-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/knotifications-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="e77886ae03b1a59d9da74a83b7945cb9e7a60992f31607d0ed545418cb384641fac8b8a6ce431c8ad5c0c57d939dab66a52a48febf2624602d4e3a6eaa3475cc  knotifications-5.94.0.tar.xz"

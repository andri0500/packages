# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kirigami-gallery
pkgver=22.04.2
pkgrel=0
pkgdesc="View examples of Kirigami components"
url="https://kde.org/applications/development/org.kde.kirigami2.gallery"
arch="all"
license="GPL-2.0-only"
depends="qt5-qtquickcontrols2 qt5-qtgraphicaleffects kirigami2 kitemmodels"
makedepends="qt5-qtbase-dev qt5-qtdeclarative-dev cmake extra-cmake-modules
	kirigami2-dev kitemmodels-dev qt5-qttools-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kirigami-gallery-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		-Bbuild .
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="b25baa9681bb8e9a43caee5329353d8f148a9340374bac2fa448aff365e4e5f255403de9553fff229871b209b1c93050f37596ce4b98a5ad7cc9bd0e1d5fc3f1  kirigami-gallery-22.04.2.tar.xz"

#!/bin/sh -e

##
# The purpose of this script is for reproducibility,
# not to be used except for creation of the bootstrap
# tarballs and to document how they were created for
# the RC3 release. This is a TEMPORARY WORKAROUND so
# that we do not unnecessarily postpone RC3 over Java.
#
# Files are uploaded to:
#
# * https://distfiles.adelielinux.org/source/openjdk/
#
# This script downloads RC2 'openjdk8' .apk files,
# extracts some JVM bits, and produces tarballs for
# bootstrapping this package, suitable for upload to
# https://distfiles.adelielinux.org/source/openjdk/.
#
# Shared libraries may be added for compatibility to
# avoid using system-provided libraries which may be
# incompatible with the existing binaries.
#
# Note: checksums may vary between 'tar' versions or
# implementations; please do not rely on this. The
# output has been verified by 'diff -qr', may differ
# in packing order, compression, or headers only.
#
# Based on https://git.adelielinux.org/-/snippets/172.
#
HERE="$(dirname $(readlink -f ${0}))";


##
# mirror for input binaries
#
host=https://distfiles.adelielinux.org;
from=1.0-rc2;


##
# supported architectures
#
# Note: empty or broken tarballs may be created for
# architectures that are missing ingredients. We do
# not treat this as an error; we want to have a full
# set of bootstrap tarballs, fix as needed, and not
# have to update code in multiple places.
#
arch="aarch64 armv7 ppc64 ppc x86_64 pmmx";


##
# packages to fetch
#
# Note: order is important; we assume tree structure
# during the manipulation stage. Does not matter for
# extraction, which we do before manipulation anyway.
# This is easily refactored but I can't be bothered.
#
apks="
user/openjdk8-jre-lib:8.252.09-r0
user/openjdk8-jre-base:8.252.09-r0
user/openjdk8-jre:8.252.09-r0
user/openjdk8:8.252.09-r0
system/libffi:3.2.1-r6
";


##
# global variables
#
vers=; # 3.2.1-r6
repo=; # system
name=; # libffi


##
# supporting routines
#
set_metadata ()
{
    vers=${apk#*:};
    repo=${apk%/*};
    temp=${apk%:${vers}};
    name=${temp#*/};
}

brk_unsupport ()
{
    printf "E: target is missing dependency '%s'.\n" "${1}";
    break;
}


##
# H. P. Lovecraft
# knows this is daft.
#
for cpu in $arch; do

    printf "\n%s\n" "$cpu";

    # output directory (specific to APKBUILD)
    keep="${HERE}"/$cpu/boot-home;

    # always start fresh
    rm -fr "${HERE}"/$cpu;
    mkdir -p "${keep}";

    # initial download/extract
    for apk in $apks; do

        set_metadata $apk;

        printf "  * %s\n" "$name-$vers.apk";

        curl -s $host/adelie/$from/$repo/$cpu/$name-$vers.apk \
            | tar -C "${HERE}"/$cpu -xzf - 2>/dev/null \
            || brk_unsupport "$name-$vers.apk"; # armv7

    done

    # optional special-case manipulation
    for apk in $apks; do

        set_metadata $apk;

        case "${name}" in
            libffi)
                # naming does not match $cpu!
                # e.g. { x86_64 --> amd64, ppc64 --> ppc, pmmx --> i386 }
                # so we look for a known file and work relative to it
                _tmp=$(find "${keep}" -name libjvm.so || brk_unsupport "libjvm.so"); # armv7
                _dst=${_tmp%/*};
                cp -a $(find "${HERE}"/$cpu/usr/lib -name "libffi.so*") \
                    "${_dst}" \
                    || brk_unsupport "libffi.so*"; # armv7
                ;;
            openjdk8)
                cp -a "${HERE}"/$cpu/usr/lib/jvm/java-1.8-openjdk \
                    "${keep}"/$cpu \
                    || brk_unsupport "openjdk8"; # armv7
                ;;
        esac

    done

    # create bootstrap tarball
    (
        cd "${HERE}"/$cpu;

        # cleanup unnecessary files (specific to APKBUILD)
        rm -fr usr;

        chown -R 1000:1000 .;
        tar -cJf "${HERE}"/openjdk8-bootstrap-$cpu.txz .;
    )

    # cleanup intermediates
    rm -fr "${HERE}"/$cpu;

done

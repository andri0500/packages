# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=qt5-qtwayland
_pkgname=qtwayland-everywhere
pkgver=5.15.4
pkgrel=0
pkgdesc="Unstable Qt platform module for experimental Wayland display system"
url="https://www.qt.io/"
arch="all"
options="!check"
license="LGPL-3.0-only WITH Qt-LGPL-exception-1.1 OR GPL-3.0-only WITH Qt-GPL-exception-1.0"
depends=""
makedepends="libxkbcommon-dev mesa-dev qt5-qtbase-dev wayland-dev"
subpackages="$pkgname-dev $pkgname-client $pkgname-compositor $pkgname-tools"
install_if="qt5-qtbase~$pkgver wayland"
source="https://download.qt.io/official_releases/qt/${pkgver%.*}/$pkgver/submodules/$_pkgname-opensource-src-$pkgver.tar.xz
	kde-lts.patch
	"
builddir="$srcdir"/$_pkgname-src-$pkgver

build() {
	qmake
	make
}

package() {
	make install INSTALL_ROOT="$pkgdir"
}

client() {
	pkgdesc="Qt client library for experimental Wayland display system"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libQt5WaylandClient* "$subpkgdir"/usr/lib/
}

compositor() {
	pkgdesc="Unstable Qt compositor library for experimental Wayland display system"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libQt5WaylandCompositor* "$subpkgdir"/usr/lib/
}

tools() {
	pkgdesc="Tools for Qt integration with experimental Wayland display system"
	mkdir -p "$subpkgdir"/usr/lib/qt5
	mv "$pkgdir"/usr/lib/qt5/bin "$subpkgdir"/usr/lib/qt5/
}

sha512sums="58ae262f7aa0455fb577a36fe9413a969398a2043160642501bac064d6fbc3280f76aa566e62b9d73c67a8c3606849b1b97bcb9b0250d26c269ec921112f40e4  qtwayland-everywhere-opensource-src-5.15.4.tar.xz
53fa6317cf27eb2201f60c6b708c244c02906ef2de157356c88bd50e917985e5162fae75da164b19d075d415508724faed92d0896f9b89d18523e7b61468cee2  kde-lts.patch"

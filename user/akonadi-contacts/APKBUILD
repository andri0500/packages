# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=akonadi-contacts
pkgver=22.04.2
pkgrel=0
pkgdesc="Library for integrating contact lists with Akonadi"
url="https://www.kde.org/"
arch="all"
options="!check"  # Tests require X11
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev akonadi-dev grantleetheme-dev kcontacts-dev
	kcoreaddons-dev kservice-dev"
makedepends="$depends_dev cmake extra-cmake-modules akonadi-mime-dev kio-dev
	grantlee-dev kcmutils-dev kmime-dev prison-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-contacts-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="81899905896da0f3b3459201400e6461d3dbec5ea2752b5176b0870974e015d03a5e409b5958c57fa737ac6b9381e9f5b347ba673d4346c32b7f84677478695c  akonadi-contacts-22.04.2.tar.xz"

# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=nano
pkgver=5.8
pkgrel=0
pkgdesc="Enhanced clone of the Pico text editor"
url="https://www.nano-editor.org"
arch="all"
license="GPL-3.0+"
depends=""
makedepends="file-dev ncurses-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://nano-editor.org/dist/v5/$pkgname-$pkgver.tar.xz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--with-wordbounds
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm644 doc/sample.nanorc \
		"$pkgdir"/etc/nanorc

	# Proper syntax highlighting for APKBUILDs
	sed -i "/syntax/s/\"$/\" \"APKBUILD&/" \
		"$pkgdir"/usr/share/nano/sh.nanorc

	rm -rf "$pkgdir"/usr/lib/charset.alias
}

sha512sums="ac614587f1a76c5ccb425fc8b4c6d4f7748dda89b863b2b8c6937b31e837edca5c83e3c13f53c9f5da5a9e24a1d8093c19dd0e8a85723f0bbae57fdab155e15c  nano-5.8.tar.xz"

# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kwalletmanager
pkgver=22.04.2
pkgrel=0
pkgdesc="Manage KDE wallets"
url="https://www.kde.org/applications/system/kwalletmanager5"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev karchive-dev kauth-dev
	kconfig-dev kconfigwidgets-dev kcoreaddons-dev kdbusaddons-dev kio-dev
	kdoctools-dev ki18n-dev kiconthemes-dev kjobwidgets-dev kcmutils-dev
	knotifications-dev kservice-dev ktextwidgets-dev kwallet-dev kcrash-dev
	kwindowsystem-dev kxmlgui-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kwalletmanager-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="63aee93199ff3380fa2d077cc29d9e5b3d63c894bd143dc7a8ed8db3339acd56d76eb59c40b98fb5cf6a616c08673226115bb38a51337ead3d010bafa9db39d9  kwalletmanager-22.04.2.tar.xz"

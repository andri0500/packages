# Maintainer: Zach van Rijn <me@zv.io>
pkgname=cups
pkgver=2.4.2
pkgrel=0
pkgdesc="The CUPS Printing System"
url="https://openprinting.github.io/cups/"
arch="all"
license="GPL-2.0-only AND LGPL-2.0-only"
# cupsUTF8ToCharset(CUPS_EUC_JP) of utfdemo.txt: FAIL (UTF-8 to EUC-JP on line 1)
options="!check"
depends="cups-client dbus openssl poppler-utils"
depends_dev="gnutls-dev openssl-dev zlib-dev"
makedepends="$depends_dev libpaper-dev dbus-dev libjpeg-turbo-dev linux-headers
	linux-pam-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs ipptool $pkgname-client
	$pkgname-lang $pkgname-openrc"
install="cups.pre-install"
pkgusers="lp"
pkggroups="lp lpadmin"
source="https://github.com/OpenPrinting/$pkgname/releases/download/v$pkgver/$pkgname-$pkgver-source.tar.gz
	$pkgname.logrotate
	cupsd.initd
	default-config-no-gssapi.patch
	"

build() {
	unset LIBTOOL; #624

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--libdir=/usr/lib \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--with-logdir=/var/log/cups \
		--with-docdir=/usr/share/cups \
		--with-rundir=/run/cups \
		--with-cupsd-file-perm=0755 \
		--with-cups-user=lp \
		--with-cups-group=lp \
		--with-system-groups=lpadmin \
		--with-domainsocket=/run/cups/cups.sock \
		--without-rcdir \
		--without-php \
		--enable-pam \
		--enable-raw-printing \
		--enable-dbus \
		--with-dbusdir=/etc/dbus-1 \
		--enable-libpaper \
		--enable-ssl=yes \
		--enable-gnutls \
		--disable-launchd \
		--with-optim="$CFLAGS"
	make
}

check() {
	make check
}

package() {
	make BUILDROOT="$pkgdir" install

	cd "$pkgdir"

	# These no longer works since CUPS >= 1.6 (http://www.cups.org/str.php?L4120).
	rm -rf usr/share/cups/banners/* \
		usr/share/cups/data/testprint

	install -D -m 644 "$srcdir"/cups.logrotate etc/logrotate.d/cups
	install -D -m 755 "$srcdir"/cupsd.initd etc/init.d/cupsd

	if [ -e usr/share/applications/cups.desktop ] ; then
		sed -i 's|^Exec=htmlview http://localhost:631/|Exec=xdg-open http://localhost:631/|g' \
			usr/share/applications/cups.desktop
	fi
	find usr/share/cups/model -name "*.ppd" | xargs gzip -n9f
}

libs() {
	pkgdesc="CUPS libraries"
	depends=""
	replaces="libcups"

	cd "$pkgdir"
	_mv usr/lib/*.so*
	install -d "$pkgdir"/etc/cups
}

ipptool() {
	pkgdesc="Perform internet printing protocol requests"
	depends=""

	cd "$pkgdir"
	_mv usr/bin/ipptool \
		usr/share/cups/ipptool
}

client() {
	pkgdesc="CUPS command-line client programs"
	depends=""

	cd "$pkgdir"
	_mv usr/bin \
		usr/sbin/cupsaccept \
		usr/sbin/cupsctl \
		usr/sbin/cupsdisable \
		usr/sbin/cupsenable \
		usr/sbin/lpadmin \
		usr/sbin/lpc \
		usr/sbin/lpinfo \
		usr/sbin/lpmove
}

_mv() {
	local i; for i in "$@"; do
		mkdir -p "$subpkgdir"/${i%/*}
		mv "$pkgdir"/$i "$subpkgdir"/${i%/*}/
	done
}

sha512sums="07474643bffe11c79b3226b70d28f1bb803dc19daa10711938cea303feacdcce3945ba8ff0334d94fdd5922ea7d6bf37a28c1ea62cce8ce946c2f90a0faf002f  cups-2.4.2-source.tar.gz
cf64211da59e79285f99d437c02fdd7db462855fb2920ec9563ba47bd8a9e5cbd10555094940ceedeb41ac805c4f0ddb9147481470112a11a76220d0298aef79  cups.logrotate
2c2683f755a220166b3a1653fdd1a6daa9718c8f0bbdff2e2d5e61d1133306260d63a83d3ff41619b5cf84c4913fae5822b79553e2822858f38fa3613f4c7082  cupsd.initd
98bb97f4af69ea286fc3d398b8e57c32440e6b2d49fb7f79b418a4fe7f13441f3a610f65d3433d10d971ade808233c0b29b4d66160623ccaae919179384be918  default-config-no-gssapi.patch"

# Contributor: Ariadne Conill <ariadne@dereferenced.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gpgme
pkgver=1.19.0
pkgrel=0
pkgdesc="GnuPG Made Easy"
url="https://www.gnupg.org/related_software/gpgme/"
arch="all"
# gpgme-tool: GPL3; lib is mixture of the rest
license="(LGPL-3.0+ OR GPL-2.0+) AND LGPL-2.1+ AND MIT AND GPL-3.0+"
depends="gnupg"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev doxygen libassuan-dev libgpg-error-dev python3-dev
	swig cmd:which"
subpackages="$pkgname-dev $pkgname-doc gpgmepp qgpgme py3-gpg:_py"
source="https://gnupg.org/ftp/gcrypt/$pkgname/$pkgname-$pkgver.tar.bz2
	initialize-err-variable.patch
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--enable-languages=cl,cpp,python,qt
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

qgpgme() {
	pkgdesc="$pkgdesc (Qt 5 library)"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libqgpgme.so* "$subpkgdir"/usr/lib/
}

gpgmepp() {
	pkgdesc="C++ bindings for GPGME"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgpgmepp.so.* "$subpkgdir"/usr/lib/
}

_py() {
	pkgdesc="$pkgdesc (Python bindings)"
	depends="$pkgname=$pkgver-r$pkgrel python3"

	mkdir -p "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/python* "$subpkgdir"/usr/lib/
}

sha512sums="c6f01ad9432abe33f407e81083dd2f299375ad13b0517429ea1c55fb8cffa05e470dd26f5910a78b8d0f4c8c1e620788a9f369d983c191a3dac681714054fe84  gpgme-1.19.0.tar.bz2
ea49e300ed5e470098d2de80c0c440ef8dd8f74363888d14527e0a5063bb85ed9f69a732f6bd9ef4013f36b4f69128035b5cf68deece10a7f9f8108c94d085f8  initialize-err-variable.patch"

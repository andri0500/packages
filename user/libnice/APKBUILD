# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libnice
pkgver=0.1.17
pkgrel=0
pkgdesc="GLib-based Interactive Connectivity Establishment (ICE) library"
url="https://nice.freedesktop.org/wiki/"
arch="all"
options="!check"  # test-send-recv fails on 32-bit platforms
license="MPL-1.1 AND LGPL-2.1-only"
depends=""
makedepends="glib-dev gobject-introspection-dev gstreamer-dev openssl-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://nice.freedesktop.org/releases/libnice-$pkgver.tar.gz
	dont-error-on-socket-close.patch
	"

build() {
	LDFLAGS="-Wl,-z,stack-size=1048576" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-static
	make
}

check() {
	# multi-make causes test-drop-invalid test to fail
	make -j1 check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="177ee47712233af379f422cbe24befaa1b744444241723a6575f17db30f7032aa8a34b1c6e160f6f406545fc42c1e7e3ca36c274bea5adb169b02434712c908e  libnice-0.1.17.tar.gz
c81aff0f8a674315997f2ecc1f0cbc501b54d49c142949aee68af42aaccf2a2f61d5eb46ce8c123b05fb98c2cd5ef5751b9228783e5e221b12be06b805da0ad3  dont-error-on-socket-close.patch"

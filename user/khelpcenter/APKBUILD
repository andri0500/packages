# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=khelpcenter
pkgver=22.04.2
pkgrel=0
pkgdesc="Graphical documentation viewer"
url="https://www.kde.org/applications/system/khelpcenter/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev karchive-dev kconfig-dev
	kbookmarks-dev kcoreaddons-dev kdbusaddons-dev kdoctools-dev ki18n-dev
	kinit-dev khtml-dev kservice-dev kwindowsystem-dev grantlee-dev
	xapian-core-dev libxml2-dev"
subpackages="$pkgname-doc $pkgname-lang"
install_if="plasma-desktop docs"
source="https://download.kde.org/stable/release-service/$pkgver/src/khelpcenter-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="acb29b9f21648010e7b30accc52b882ddf8702a58695b2a316126d45425f3b903f289d56b3fe278a0604d312e663d448c4e0143e58f6f82fb616811e83c007e9  khelpcenter-22.04.2.tar.xz"

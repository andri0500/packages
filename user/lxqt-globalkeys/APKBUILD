# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=lxqt-globalkeys
pkgver=1.2.0
_lxqt=0.12.0
pkgrel=0
pkgdesc="Daemon and configuration utility for global shortcuts in LXQt"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules lxqt-build-tools>=$_lxqt
	liblxqt-dev>=${pkgver%.*}.0 qt5-qttools-dev kwindowsystem-dev"
subpackages="$pkgname-dev"
source="https://github.com/lxqt/lxqt-globalkeys/releases/download/$pkgver/lxqt-globalkeys-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="c1270ae03fcb2fd8b8e548898c36d57ead6b5bea62f246a1587f4093a937141c6c7339f838d1f4ff0b9bd9b9c8079ba8cf0229b575d323e6d78e8b7ccc2eb624  lxqt-globalkeys-1.2.0.tar.xz"

# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=s6-dns
pkgver=2.3.5.5
pkgrel=0
pkgdesc="skarnet.org's DNS client libraries and command-line DNS client utilities"
url="https://skarnet.org/software/s6-dns/"
arch="all"
options="!check"
license="ISC"
_skalibs_version=2.13
depends=""
makedepends="skalibs-dev>=$_skalibs_version skalibs-libs-dev>=$_skalibs_version"
subpackages="$pkgname-libs $pkgname-dev $pkgname-libs-dev:libsdev $pkgname-doc"
source="https://skarnet.org/software/$pkgname/$pkgname-$pkgver.tar.gz"

build() {
	./configure \
		--enable-shared \
		--enable-static \
		--disable-allstatic \
		--prefix=/usr \
		--libdir=/usr/lib \
		--libexecdir="/usr/lib/$pkgname" \
		--with-dynlib=/lib
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

libs() {
	pkgdesc="$pkgdesc (shared libraries)"
	depends="skalibs-libs>=$_skalibs_version"
	mkdir -p "$subpkgdir/usr/lib"
	mv "$pkgdir"/usr/lib/*.so.* "$subpkgdir/usr/lib/"
}

dev() {
	pkgdesc="$pkgdesc (development files)"
	depends="skalibs-dev>=$_skalibs_version"
	install_if="dev $pkgname=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir/usr/include" "$subpkgdir/usr/lib"
	mv "$pkgdir/usr/include" "$subpkgdir/usr/"
	mv "$pkgdir"/usr/lib/*.a "$subpkgdir/usr/lib/"
}

libsdev() {
	pkgdesc="$pkgdesc (development files for dynamic linking)"
	depends="$pkgname-dev"
	mkdir -p "$subpkgdir/usr/lib"
	mv "$pkgdir"/usr/lib/*.so "$subpkgdir/usr/lib/"
}

doc() {
	default_doc
	mkdir -p "$subpkgdir/usr/share/doc"
	cp -a "$builddir/doc" "$subpkgdir/usr/share/doc/$pkgname"
}

sha512sums="7d7435da8fab3dd8f872c4dd90338fae2b817b4cb1e4715cc86d77b4c1f58e931bdeb1385093f6bd6293951f97f765fb6372af5560042ae84b5bd53ecc2645c2  s6-dns-2.3.5.5.tar.gz"

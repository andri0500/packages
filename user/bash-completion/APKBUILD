# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Contributor: zlg <zlg+adelie@zlg.space>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=bash-completion
pkgver=2.11
pkgrel=0
pkgdesc="Command-line tab-completion for the Bash shell"
url="https://github.com/scop/bash-completion"
arch="noarch"
license="GPL-2.0+"
depends="bash"
checkdepends="py3-pexpect py3-pytest py3-iniconfig"
makedepends=""
subpackages="$pkgname-doc"
source="https://github.com/scop/${pkgname}/releases/download/${pkgver}/${pkgname}-${pkgver}.tar.xz
	fix-tests.patch
	"

# Provided with util-linux and networkmanager:
_conflicting="
	cal
	chsh
	dmesg
	eject
	hd
	hexdump
	hwclock
	ionice
	look
	ncal
	newgrp
	renice
	rfkill
	rtcwake
	su
	nmcli
	umount
	mount
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	for k in \
		test_aclocal \
		test_chsh \
		test_hostname \
		test_ifdown \
		test_ifup \
		test_man \
		test_service \
		test_xmlwf \
		unit/test_unit_expand \
		unit/test_unit_expand_tilde_by_ref \
		; do
		rm test/t/${k}.py
	done
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install

	mkdir -p "$pkgdir"/usr/share/doc/$pkgname/
	install -m644 AUTHORS CHANGES CONTRIBUTING.md README.md \
		"$pkgdir"/usr/share/doc/$pkgname/

	cd "$pkgdir"/usr/share/bash-completion/completions
	for c in $_conflicting; do
		rm -f $c
	done
}

sha512sums="41585f730b5114d397831ba36d10d05643c6a6179e746ddc49aa1cbef61ea5525fd2f09b2e474adee14e647f99df8d5983ee48e29a59d8a30e1daf7fb1837e06  bash-completion-2.11.tar.xz
ada80cbec419a6f6b2eab648497d5aa6795883bc52a160c233c04d4479c6d543b5089745ab098c5d6d7b4a0f05d25708cf7cf9d3b565b5b70e96796b3042ca75  fix-tests.patch"

# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=mpv
pkgver=0.34.1
pkgrel=0
pkgdesc="An improved fork of mplayer"
url="https://mpv.io"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+ AND GPL-2.0+ AND ISC AND BSD-2-Clause AND MIT AND BSD-3-Clause"
depends=""
makedepends="python3
	zlib-dev libarchive-dev py3-docutils uchardet-dev ncurses-dev
	
	alsa-lib-dev pulseaudio-dev

	libx11-dev libxext-dev libxinerama-dev libxrandr-dev libxscrnsaver-dev
	mesa-dev libva-dev lcms2-dev libvdpau-dev

	ffmpeg-dev libbluray-dev v4l-utils-dev libass-dev libdvdread-dev
	libdvdnav-dev libcdio-dev libcdio-paranoia-dev rubberband-dev
	"
subpackages="$pkgname-doc"
source="mpv-$pkgver.tar.gz::https://github.com/mpv-player/mpv/archive/v$pkgver.tar.gz"

# secfixes:
#   0.34.1-r0:
#     - CVE-2021-30145

build() {
	python3 ./bootstrap.py
	python3 ./waf configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--destdir="$pkgdir" \
		--disable-lua \
		--disable-javascript \
		--disable-wayland \
		--disable-gl-wayland \
		--enable-dvdnav \
		--enable-cdda
	python3 ./waf build
}

package() {
	python3 ./waf install --destdir="$pkgdir"
}

sha512sums="77ea349d6999f8cce9b5cce4cebd3506a224fc18ab08d22dd16bd34c34d012bb170879b268ddd62db40d116b4cc0b2d9d651b8097f387ed9115c426834cac77e  mpv-0.34.1.tar.gz"

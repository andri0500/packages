# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kwordquiz
pkgver=22.04.2
pkgrel=0
pkgdesc="Flash card trainer for KDE"
url="https://www.kde.org/applications/education/kwordquiz/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev ki18n-dev kcrash-dev
	kconfig-dev kconfigwidgets-dev kdoctools-dev kguiaddons-dev
	kiconthemes-dev kitemviews-dev knotifyconfig-dev knewstuff-dev
	knotifications-dev kxmlgui-dev kdelibs4support-dev
	libkeduvocdocument-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kwordquiz-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="ccb67e5c10137257ffbb24a109222121fb83c0df54e94a8b59f06d6de859253fc5896b82d6f3c6b1bbfa87502e058f02d251a092c27bf31886a4ca11bf474f3f  kwordquiz-22.04.2.tar.xz"

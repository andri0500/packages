# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=ethtool
pkgver=5.4
pkgrel=0
pkgdesc="Utility for controlling network drivers and hardware"
url="https://mirrors.edge.kernel.org/pub/software/network/ethtool/"
arch="all"
license="GPL-2.0-only AND GPL-2.0+"
depends=""
makedepends="linux-headers"
subpackages="$pkgname-doc $pkgname-bash-completion:bashcomp:noarch"
source="https://www.kernel.org/pub/software/network/ethtool/$pkgname-$pkgver.tar.xz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

bashcomp() {
	pkgdesc="Bash completions for $pkgname"
	depends=""
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	mkdir -p "$subpkgdir"/usr/share/
	mv "$pkgdir"/usr/share/bash-completion/ "$subpkgdir"/usr/share/
}

sha512sums="c7bfe7eae2ba207d710716274e9f5ae275c57d0bebc0a9acb95677125e64f1b42ef610032011fb59894aab46973988bad40fddbc5ccc238893e7da66f913255b  ethtool-5.4.tar.xz"

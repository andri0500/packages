# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Michael Mason <ms13sp@gmail.com>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Lee Starnes <lee@canned-death.us>
pkgname=dovecot
pkgver=2.3.20
case "$pkgver" in
	*.*.*.*) _pkgvermajor=${pkgver%.*.*};;
	*.*.*) _pkgvermajor=${pkgver%.*};;
esac
pkgrel=0
_pigeonholever=0.5.20
pkgdesc="IMAP and POP3 server"
url="https://www.dovecot.org/"
arch="all"
options="libtool"
license="LGPL-2.0+"
depends="openssl"
makedepends="autoconf automake libtool bzip2-dev expat-dev krb5-dev libcap-dev
	libsodium-dev linux-headers linux-pam-dev mariadb-dev openldap-dev
	openssl-dev postgresql-dev sqlite-dev zlib-dev"
pkgusers="dovecot dovenull"
pkggroups="dovecot dovenull"
install="$pkgname.pre-install $pkgname.post-install $pkgname.post-upgrade"
subpackages="$pkgname-doc $pkgname-dev $pkgname-openrc $pkgname-lmtpd
	$pkgname-pop3d $pkgname-submissiond
	$pkgname-pigeonhole-plugin-ldap:_sieve_ldap
	$pkgname-pigeonhole-plugin:_sieve
	$pkgname-sql $pkgname-pgsql $pkgname-mysql $pkgname-sqlite
	$pkgname-gssapi $pkgname-ldap $pkgname-fts-solr:_fts_solr
	"
source="https://www.dovecot.org/releases/$_pkgvermajor/dovecot-$pkgver.tar.gz
	https://pigeonhole.dovecot.org/releases/$_pkgvermajor/$pkgname-$_pkgvermajor-pigeonhole-$_pigeonholever.tar.gz
	fix-time64.patch
	skip-iconv-check.patch
	split-protocols.patch
	default-config.patch
	ssl-paths.patch
	test-file-cache-enomem.patch
	dovecot.logrotate
	dovecot.initd
	CVE-2022-30550.patch
	"
_builddir_pigeonhole="$srcdir/$pkgname-$_pkgvermajor-pigeonhole-$_pigeonholever"

# secfixes:
#   2.3.19.1-r0:
#     - CVE-2022-30550
#   2.3.19-r0:
#     - CVE-2020-28200
#     - CVE-2021-29157
#     - CVE-2021-33515
#   2.3.13-r0:
#     - CVE-2020-24386
#     - CVE-2020-25275
#   2.3.11.3-r0:
#     - CVE-2020-12100
#     - CVE-2020-12673
#     - CVE-2020-12674
#   2.3.10.1-r0:
#     - CVE-2020-10957
#     - CVE-2020-10958
#     - CVE-2020-10967

_configure() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--localstatedir=/var \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-static \
		"$@"
}

build() {
	_configure \
		--with-gssapi=plugin \
		--with-ldap=plugin \
		--with-sql=plugin \
		--with-pam \
		--with-mysql \
		--with-sqlite \
		--with-pgsql \
		--with-solr \
		--with-ssl=openssl \
		--with-ssldir=/etc/ssl/dovecot \
		--with-rundir=/run/dovecot \
		--without-lucene
	make

	# Build pigeonhole plugin
	cd "$_builddir_pigeonhole"
	_configure \
		--with-dovecot="$builddir" \
		--with-ldap=plugin
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	cd "$pkgdir"

	install -d ./etc/ssl/dovecot
	install -D -m 755 "$srcdir"/dovecot.initd ./etc/init.d/dovecot
	install -D -m 644 "$srcdir"/dovecot.logrotate ./etc/logrotate.d/dovecot

	# default config
	rm ./etc/dovecot/*
	rm ./usr/share/doc/dovecot/mkcert.sh
	mv ./usr/share/doc/dovecot/dovecot-openssl.cnf ./etc/dovecot/
	mv ./usr/share/doc/dovecot/example-config/dovecot* \
		./usr/share/doc/dovecot/example-config/conf.d \
		./etc/dovecot/
	rm -fr ./usr/share/doc/dovecot/example-config

	# Installing pigeonhole plugin.
	cd "$_builddir_pigeonhole"
	make install DESTDIR="$pkgdir"

	# Moving config in the correct place
	mv "$pkgdir"/usr/share/doc/dovecot/example-config/conf.d/* \
		"$pkgdir"/etc/dovecot/conf.d

	# Remove libtool archives. abuild doesn't remove them automatically even without options=libtool.
	find "$pkgdir" -name '*.la' | xargs rm -f
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr/lib/dovecot
	mv "$pkgdir"/usr/lib/dovecot/dovecot-config \
		"$subpkgdir"/usr/lib/dovecot/
}

lmtpd() {
	pkgdesc="$pkgdesc (LMTP server)"
	_protocol lmtp
}

pop3d() {
	pkgdesc="$pkgdesc (POP3 daemon)"
	_protocol pop3
}

submissiond() {
	pkgdesc="$pkgdesc (mail submission agent)"
	_protocol submission
}

_protocol() {
	depends="$pkgname=$pkgver-r$pkgrel"
	_name="$1"
	_protocolsd="$subpkgdir/usr/share/dovecot/protocols.d"

	cd "$pkgdir"
	_submv usr/libexec/dovecot/$_name*
	_submv etc/dovecot/conf.d/*-$_name.conf

	mkdir -p "$_protocolsd"
	echo "protocols = \$protocols $_name" \
		> "$_protocolsd"/${subpkgname#$pkgname-}.conf
}

_sieve() {
	pkgdesc="Sieve and managesieve plugin for Dovecot"
	depends="$pkgname=$pkgver-r$pkgrel"

	cd "$pkgdir"
	_submv $(find usr/ -name '*sieve_extprograms*')
	_submv $(find usr/ -name '*sieve_imapsieve*')
	_submv $(find usr/ -name '*sieve*')
	_submv $(find usr/ -name '*pigeonhole*')
	_submv $(find etc/dovecot/ -name '*sieve*')
}

_sieve_ldap() {
	pkgdesc="Sieve and managesieve plugin for Dovecot (LDAP support)"
	depends="$pkgname-pigeonhole-plugin=$pkgver-r$pkgrel $pkgname-ldap=$pkgver-r$pkgrel"

	cd "$pkgdir"
	_submv $(find usr/ -name '*_sieve_storage_ldap_*')
}

pgsql() {
	pkgdesc="PostgreSQL driver for Dovecot"
	depends="$pkgname-sql=$pkgver-r$pkgrel"

	cd "$pkgdir"
	_submv $(find usr/ -name '*_pgsql*')
}

mysql() {
	pkgdesc="MySQL driver for Dovecot"
	depends="$pkgname-sql=$pkgver-r$pkgrel"

	cd "$pkgdir"
	_submv $(find usr/ -name '*_mysql*')
}

sqlite() {
	pkgdesc="SQLite driver for Dovecot"
	depends="$pkgname-sql=$pkgver-r$pkgrel"

	cd "$pkgdir"
	_submv $(find usr/ -name '*_sqlite*')
}

gssapi() {
	pkgdesc="GSSAPI auth plugin for Dovecot"
	depends="$pkgname=$pkgver-r$pkgrel"

	cd "$pkgdir"
	_submv $(find usr/ -name '*_gssapi*')
}

ldap() {
	pkgdesc="LDAP auth plugin for Dovecot"
	depends="$pkgname=$pkgver-r$pkgrel"

	cd "$pkgdir"
	_submv $(find usr/ -name '*[_-]ldap*')
	_submv $(find etc/dovecot/ -name '*-ldap.conf*')
}

sql() {
	pkgdesc="SQL plugin for dovecot"
	depends="$pkgname=$pkgver-r$pkgrel"

	cd "$pkgdir"
	_submv $(find usr/ -name '*-sql.*')
	_submv $(find etc/dovecot/ -name '*-sql.conf*')
}

_fts_solr() {
	pkgdesc="FTS-Solr plugin for dovecot"
	depends="$pkgname=$pkgver-r$pkgrel"

	cd "$pkgdir"
	_submv $(find usr/ -name '*fts*solr*')
}

_submv() {
	while [ $# -gt 0 ]; do
		_dir=${1%/*}
		mkdir -p "$subpkgdir"/$_dir
		mv "$pkgdir/$1" "$subpkgdir/$_dir/"
		[ "$(ls -A $pkgdir/$_dir)" ] || rmdir "$pkgdir"/$_dir
		shift
	done
}

sha512sums="20c5a9cacf2c22d99d46400b666206e5b153c35286c205eec5df4d2ce0c88cf29ea15df81716794fd75837f6d67dfa4037096cf4bb66f524877a9a0a6bb282c8  dovecot-2.3.20.tar.gz
45683e6bd678db00fc3e3c61d27a264d30d0e9aeb9ceb7ab55f94f0317d387056fa092e266062117cbe2a9dc2c90ddca03d154e78aad9c0d61fe8cf2c9187603  dovecot-2.3-pigeonhole-0.5.20.tar.gz
a73e7323a582ea9504c33ea3591784af8187af8fffbcc47b69f549e33620c532d976853f0f7a9071af6ef2970d4899e2c5b30aac605dd5c933b3c6faa391bad4  fix-time64.patch
fe4fbeaedb377d809f105d9dbaf7c1b961aa99f246b77189a73b491dc1ae0aa9c68678dde90420ec53ec877c08f735b42d23edb13117d7268420e001aa30967a  skip-iconv-check.patch
794875dbf0ded1e82c5c3823660cf6996a7920079149cd8eed54231a53580d931b966dfb17185ab65e565e108545ecf6591bae82f935ab1b6ff65bb8ee93d7d5  split-protocols.patch
0d8f89c7ba6f884719b5f9fc89e8b2efbdc3e181de308abf9b1c1b0e42282f4df72c7bf62f574686967c10a8677356560c965713b9d146e2770aab17e95bcc07  default-config.patch
5e68a0042a7c11b3d8c411fc157f5960e2e3305dac11f4b6b880441e2b4105769ddf6c56f67a995af6e1a58f3bfa2c199ea51318a3a0e37c7ef0ae6c4109b13f  ssl-paths.patch
277d0b55583908cd6c063fb190eb3d2a362a3a33306e35a069a0bca28968f11627c455887d82d01884313b7f7e18530c056bd7a4cda0a1f9c4be7065cd033aa9  test-file-cache-enomem.patch
9f19698ab45969f1f94dc4bddf6de59317daee93c9421c81f2dbf8a7efe6acf89689f1d30f60f536737bb9526c315215d2bce694db27e7b8d7896036a59c31f0  dovecot.logrotate
d91951b81150d7a3ef6a674c0dc7b012f538164dac4b9d27a6801d31da6813b764995a438f69b6a680463e1b60a3b4f2959654f68e565fe116ea60312d5e5e70  dovecot.initd
b2ff67fd8b6c5cea93877651a1168ef1a5d399cc5f1a61d1cce407c7624f5b6d758996084c6a5714b6880de0ce11ce5eac74a1e02918758cb6983caedb651c58  CVE-2022-30550.patch"

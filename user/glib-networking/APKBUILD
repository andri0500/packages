# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=glib-networking
pkgver=2.70.1
_ver=${pkgver%.*}
pkgrel=0
pkgdesc="Networking support for GLib"
url="https://www.gnome.org/"
arch="all"
license="LGPL-2.1+"
depends="ca-certificates"
makedepends="bash glib-dev gnutls-dev gsettings-desktop-schemas-dev intltool
	libgcrypt-dev libproxy-dev meson ninja p11-kit-dev"
subpackages="$pkgname-lang"
source="https://download.gnome.org/sources/glib-networking/$_ver/glib-networking-$pkgver.tar.xz
	tls13.patch
	disable-gnome-test.patch
	"

build() {
	meson -Dprefix=/usr _build
	ninja -C _build
}

check() {
	ninja -C _build test
}

package() {
	DESTDIR="$pkgdir" ninja -C _build install
	rm -f "$pkgdir"/usr/lib/gio/modules/*.a
}

sha512sums="a06b4df4481f95193f9ed4be6d39bbe9ecaf4de8e11a48750f7110d4cfa71aa56b7ec5b36af70b7128150447f1a39ce3aeadf71e2ac516f61708f1212f8f855d  glib-networking-2.70.1.tar.xz
b859a3053013b54e62139e6bdd4e5c0d318ff9fcbc71533d35a3fd9efccfc0cefa8b070cc1047c67549efc01623829a07d47ad73d1fb8aeb106cf57c975034ab  tls13.patch
ebd90e59cfe12b993792855f6059918f13d5f7847c7db2b1b52aad868c1ba119941f319ed1b76b8fffd221734d8169356c682013c588452ee2ce0c02c3c5cbab  disable-gnome-test.patch"

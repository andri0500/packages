# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=breeze-icons
pkgver=5.94.0
pkgrel=0
pkgdesc="Modern, coherent icon set for desktops"
url="https://www.kde.org/"
arch="noarch"
options="!check"  # 8,753 failures because it can't tell symlink from file.
license="LGPL-3.0+"
depends=""
checkdepends="fdupes"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev"
subpackages="breeze-icons-dark:dark"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/breeze-icons-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		-B build \
		.
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

dark() {
	pkgdesc="Modern, coherent icon set for desktops - Darker and edgier"
	mkdir -p "$subpkgdir"/usr/share/icons
	mv "$pkgdir"/usr/share/icons/breeze-dark "$subpkgdir"/usr/share/icons/
}

sha512sums="63f58c75864b3791c7b7ab87666c49328589cd2867f166e496553281497375d9c6054c3ddfea7c6101092d8636d35d4c263fdccd3a58b50d9dd2a14210facb5c  breeze-icons-5.94.0.tar.xz"

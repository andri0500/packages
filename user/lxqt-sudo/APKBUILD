# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=lxqt-sudo
pkgver=1.2.0
_lxqt=0.12.0
pkgrel=0
pkgdesc="Graphical LXQt utility for sudo/su"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends="sudo"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev lxqt-build-tools>=$_lxqt
	liblxqt-dev>=${pkgver%.*}.0 qt5-qttools-dev kwindowsystem-dev"
subpackages="$pkgname-doc"
source="https://github.com/lxqt/lxqt-sudo/releases/download/$pkgver/lxqt-sudo-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="4b2d91e4f6d364c583ec4db893469628000369b08438f4ea26428621b929f26770c8f8b4b756acd865b404cb2a5e6921161527ff133394a90f0592226bfe3ea8  lxqt-sudo-1.2.0.tar.xz"

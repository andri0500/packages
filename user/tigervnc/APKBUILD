# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=tigervnc
pkgver=1.10.1
pkgrel=0
pkgdesc="High-performance, platform-neutral VNC remote desktop application"
url="https://tigervnc.org/"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0"
depends=""
makedepends="cmake fltk-dev fontconfig-dev gnutls-dev intltool
	libjpeg-turbo-dev libx11-dev libxcursor-dev libxfixes-dev libxft-dev
	libxrender-dev libxtst-dev linux-pam-dev zlib-dev"
subpackages="$pkgname-lang $pkgname-doc"
source="tigervnc-$pkgver.tar.gz::https://github.com/TigerVNC/tigervnc/archive/v$pkgver.tar.gz
	use-intltool.patch
	0001-CSecurityTLS-Use-size_t-as-argument-for-new.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS -faligned-new -Wno-c++11-compat -Wno-maybe-uninitialized" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-Wno-dev \
		${CMAKE_CROSSOPTS} .
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="3486ff772d39217feb8c075860cae58e1021bdb8095443d8b5c383929cc6c622b57ea61c31d06ff0bd48c7e6889db4b6a99d0742bdd60edf323fb2a3ad328705  tigervnc-1.10.1.tar.gz
5c1cee98b7ba41c7cf121480fdfe16d5ef17c9562ff2ba3ea4e74235161fc63e2e3ed63e788c0aa999610b660b394c1269d6fdcc9716c5563651fd67d723f619  use-intltool.patch
f95328f6b669e6608b9971de3db25d5eb26a733fbe32f13291c309ed57eacba6c86461a516c3b8cdc12ff7482ee0249a45189864d473d52df81df0a3541d95b9  0001-CSecurityTLS-Use-size_t-as-argument-for-new.patch"

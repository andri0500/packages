# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=konsole
pkgver=22.04.2
pkgrel=0
pkgdesc="Terminal emulator for Qt/KDE"
url="https://konsole.kde.org/"
arch="all"
options="!check"  # Requires running DBus session bus.
license="GPL-2.0-only AND LGPL-2.1+ AND Unicode-DFS-2016"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kbookmarks-dev
	kcompletion-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev
	kcrash-dev kguiaddons-dev kdbusaddons-dev ki18n-dev kiconthemes-dev
	kinit-dev kio-dev knotifications-dev knotifyconfig-dev kparts-dev
	kpty-dev kservice-dev ktextwidgets-dev kwidgetsaddons-dev python3
	kwindowsystem-dev kxmlgui-dev kdbusaddons-dev knewstuff-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/konsole-$pkgver.tar.xz
	musl.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="cd8ecc3e494cc2c087e03b4537b822986d36a04d75edf9c8d053a79578e148498582238f96018262cc74cdf86fe7abf7a0a4408e220ec0a9a49bb94d8aa10a76  konsole-22.04.2.tar.xz
7f39e9e026cc17214eb0338f90fd9e2c007875813a0fdbd1fae421b779083d94ebe83df5226fc67b451ac3ab3f8aa9501f996551519c718ca23c3de5a7c16fbe  musl.patch"

# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kwindowsystem
pkgver=5.94.0
pkgrel=0
pkgdesc="Framework for introspecting the running X11 window manager"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires X11 running
license="MIT AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends=""
depends_dev="qt5-qtbase-dev libx11-dev libxcb-dev xcb-util-keysyms-dev
	xcb-util-wm-dev qt5-qtx11extras-dev libxrender-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev doxygen
	graphviz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwindowsystem-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="908f8bffdfbcc17a605a3e46c652508859b34d2990a171509af8c3b886f62a0c9724697f82e08949625e407c4daa6b4bd5ed80d949b6365060c17bc01e8a5944  kwindowsystem-5.94.0.tar.xz"

# Contributor: Travis Tilley <ttilley@gmail.com>
# Contributor: Mitch Tishmack <mitch.tishmack@gmail.com>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
_pkgname=llvm
pkgver=14.0.6
_majorver=${pkgver%%.*}
pkgname=$_pkgname$_majorver
pkgrel=0
pkgdesc="Low Level Virtual Machine compiler system, version $_majorver"
url="https://llvm.org/"
arch="all"
options="!checkroot !dbg"
license="NCSA AND (Apache-2.0 WITH LLVM-exception)"
depends=""
depends_dev="$pkgname=$pkgver-r$pkgrel libexecinfo-dev libxml2-dev"
checkdepends="cmd:which"
makedepends="binutils-dev chelf chrpath cmake file libexecinfo-dev libffi-dev
	libxml2-dev python3 zlib-dev"
subpackages="$pkgname-static $pkgname-libs $pkgname-dev
	$pkgname-test-utils:_test_utils"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/llvm-$pkgver.src.tar.xz
	llvm-fix-build-with-musl-libc.patch
	disable-FileSystemTest.CreateDir-perms-assert.patch
	disable-dlclose-test.patch
	dwarf-info.patch
	dyld-elf-ppc32.patch
	hexagon.patch
	macho32.patch
	musl-ppc64-elfv2.patch
	ppc-test.patch
	python3-test.patch
	roundeven.patch
	"
builddir="$srcdir/$_pkgname-$pkgver.src"

# ARM has few failures in test suite that we don't care about currently and
# also it takes forever to run them on the builder.
case "$CARCH" in
	arm*) options="$options !check";;
esac

# Whether is this package the default (latest) LLVM version.
_default_llvm="yes"

if [ "$_default_llvm" = yes ]; then
	provides="llvm=$pkgver-r$pkgrel"
	replaces="llvm"
fi

# NOTE: It seems that there's no (sane) way how to change includedir, sharedir
# etc. separately, just the CMAKE_INSTALL_PREFIX. Standard CMake variables and
# even  LLVM-specific variables, that are related to these paths, actually
# don't work (in llvm 3.7).
#
# When building a software that depends on LLVM, utility llvm-config should be
# used to discover where is LLVM installed. It provides options to print
# path of bindir, includedir, and libdir separately, but in its source, all
# these paths are actually hard-coded against INSTALL_PREFIX. We can patch it
# and move paths manually, but I'm really not sure what it may break...
#
# Also note that we should *not* add version suffix to files in llvm bindir!
# It breaks build system of some software that depends on LLVM, because they
# don't expect these files to have a sufix.
#
# So, we install all the LLVM files into /usr/lib/llvm$_majorver.
# BTW, Fedora and Debian do the same thing.
#
_prefix="usr/lib/llvm$_majorver"

prepare() {
	default_prepare

	# https://bugs.llvm.org//show_bug.cgi?id=31870
	rm test/tools/llvm-symbolizer/print_context.c

	case $CARCH in
	pmmx|x86|ppc|armhf|armv7)
		# Appears to not work when building 32-bit code on 64-bit host.
		rm test/tools/llvm-size/radix.test;;
	esac
}

build() {
	# Auto-detect it by guessing either.
	ffi_include_dir="$(pkg-config --cflags-only-I libffi | sed 's|^-I||g')"

	cmake -G "Unix Makefiles" -Wno-dev \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/$_prefix \
		-DCMAKE_VERBOSE_MAKEFILE=NO \
		-DFFI_INCLUDE_DIR="$ffi_include_dir" \
		-DLLVM_BINUTILS_INCDIR=/usr/include \
		-DLLVM_BUILD_DOCS=OFF \
		-DLLVM_BUILD_EXAMPLES=OFF \
		-DLLVM_BUILD_EXTERNAL_COMPILER_RT=ON \
		-DLLVM_BUILD_LLVM_DYLIB=ON \
		-DLLVM_BUILD_TESTS=ON \
		-DLLVM_DEFAULT_TARGET_TRIPLE="$CBUILD" \
		-DLLVM_ENABLE_ASSERTIONS=OFF \
		-DLLVM_ENABLE_FFI=ON \
		-DLLVM_ENABLE_LIBCXX=OFF \
		-DLLVM_ENABLE_PIC=ON \
		-DLLVM_ENABLE_RTTI=ON \
		-DLLVM_ENABLE_SPHINX=OFF \
		-DLLVM_ENABLE_TERMINFO=ON \
		-DLLVM_ENABLE_ZLIB=ON \
		-DLLVM_HOST_TRIPLE="$CHOST" \
		-DLLVM_INCLUDE_BENCHMARKS=OFF \
		-DLLVM_INCLUDE_EXAMPLES=OFF \
		-DLLVM_INSTALL_UTILS=ON \
		-DLLVM_LINK_LLVM_DYLIB=ON \
		-DLLVM_TARGETS_TO_BUILD='AArch64;AMDGPU;ARM;BPF;Hexagon;Lanai;Mips;MSP430;PowerPC;RISCV;Sparc;SystemZ;WebAssembly;X86;XCore' \
		-DLLVM_EXPERIMENTAL_TARGETS_TO_BUILD='M68k' \
		-Bbuild .

	make -C build llvm-tblgen
	make -C build

	python3 utils/lit/setup.py build
}

check() {
	# appears to be an issue on musl and glibc, but only fails on musl:
	# https://github.com/NixOS/nixpkgs/blob/bb7e9e46/pkgs/development/compilers/llvm/8/llvm.nix#L74
	rm -f "$builddir"/test/CodeGen/AArch64/wineh4.mir
	make -C build check-llvm
}

package() {
	make DESTDIR="$pkgdir" -C build install

	cd "$pkgdir"/$_prefix

	# Remove RPATHs.
	file lib/*.so bin/* \
		| awk -F: '$2~/ELF/{print $1}' \
		| xargs -r chrpath -d

	# Thread 3 requires a lot of stack space to LTO when targeting ARM.
	# Note that this occurs even when crossing (on a ppc64 host).
	chelf -s 1048576 bin/llvm-lto2

	# Symlink files from /usr/lib/llvm*/bin to /usr/bin.
	mkdir -p "$pkgdir"/usr/bin
	for full_name in bin/*; do
		bin_name=${full_name##*/}
		# If this package provides=llvm (i.e. it's the default/latest
		# llvm package), omit version infix/suffix.
		if [ "$_default_llvm" = yes ]; then
			link_name=$bin_name
		# ..otherwise, add version infix/suffix to the executable name.
		else case "$bin_name" in
			llvm-*) link_name="llvm$_majorver-${bin_name#llvm-}";;
			*) link_name="$bin_name$_majorver";;
		esac
		fi
		case "$name" in
			FileCheck | obj2yaml | yaml2obj) continue;;
		esac
		ln -s ../lib/llvm$_majorver/bin/$bin_name "$pkgdir"/usr/bin/$link_name
	done

	# Move /usr/lib/$pkgname/include/ into /usr/include/$pkgname/
	# and symlink it back.
	mkdir -p "$pkgdir"/usr/include/$pkgname
	mv include/* "$pkgdir"/usr/include/$pkgname/
	rmdir include
	ln -s ../../include/$pkgname include

	ln -s "$pkgdir"/usr/lib/cmake/llvm ../$pkgname/lib/cmake/llvm
}

static() {
	pkgdesc="LLVM $_majorver static libraries"
	_common_subpkg

	mkdir -p "$subpkgdir"/$_prefix/lib
	mv "$pkgdir"/$_prefix/lib/*.a "$subpkgdir"/$_prefix/lib/
	strip -d "$subpkgdir"/$_prefix/lib/*.a
}

libs() {
	pkgdesc="LLVM $_majorver runtime library"
	main_soname="libLLVM-$_majorver.so"
	ver_soname="libLLVM-$pkgver.so"
	_common_subpkg

	# libLLVM should be in /usr/lib. This is needed for binaries that are
	# dynamically linked with libLLVM, so they can find it on default path.
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/$_prefix/lib/$main_soname "$subpkgdir"/usr/lib/
	ln -s $main_soname "$subpkgdir"/usr/lib/$ver_soname
	strip "$subpkgdir"/usr/lib/$ver_soname #797

	# And also symlink it back to the LLVM prefix.
	mkdir -p "$subpkgdir"/$_prefix/lib
	ln -s ../../$main_soname "$subpkgdir"/$_prefix/lib/$main_soname
	ln -s ../../$main_soname "$subpkgdir"/$_prefix/lib/$ver_soname
}

dev() {
	_common_subpkg
	default_dev

	mkdir -p "$subpkgdir"/$_prefix/bin

	mv "$pkgdir"/$_prefix/lib "$subpkgdir"/$_prefix/
	mv "$pkgdir"/$_prefix/include "$subpkgdir"/$_prefix/

	mv "$pkgdir"/$_prefix/bin/llvm-config "$subpkgdir"/$_prefix/bin/
}

_test_utils() {
	pkgdesc="LLVM $_majorver utilities for executing LLVM and Clang style test suites"
	depends="python3"
	_common_subpkg
	replaces=""

	litver="$(python3 "$builddir"/utils/lit/setup.py --version 2>/dev/null \
		| sed 's/\.dev.*$//')"
	test -n "$litver" || return 1
	provides="$provides lit=$litver-r$pkgrel"

	cd "$builddir"/build

	install -D -m 755 bin/FileCheck "$subpkgdir"/$_prefix/bin/FileCheck
	install -D -m 755 bin/count "$subpkgdir"/$_prefix/bin/count
	install -D -m 755 bin/not "$subpkgdir"/$_prefix/bin/not

	python3 ../utils/lit/setup.py install --prefix=/usr --root="$subpkgdir"
	ln -s ../../../bin/lit "$subpkgdir"/$_prefix/bin/lit
}

_common_subpkg() {
	if [ "$_default_llvm" = yes ]; then
		replaces="llvm${subpkgname#$pkgname}"
		provides="$replaces=$pkgver-r$pkgrel"
	fi
}

sha512sums="6461bdde27aac17fa44c3e99a85ec47ffb181d0d4e5c3ef1c4286a59583e3b0c51af3c8081a300f45b99524340773a3011380059e3b3a571c3b0a8733e96fc1d  llvm-14.0.6.src.tar.xz
f84cd65d7042e89826ba6e8d48c4c302bf4980da369d7f19a55f217e51c00ca8ed178d453df3a3cee76598a7cecb94aed0775a6d24fe73266f82749913fc3e71  llvm-fix-build-with-musl-libc.patch
49c47f125014b60d0ea7870f981a2c1708ad705793f89287ed846ee881a837a4dc0170bf467e03f2ef56177473128945287749ac80dc2d13cfabcf8b929ba58a  disable-FileSystemTest.CreateDir-perms-assert.patch
caeec8e4dbd92f5f74940780b69075f3879a267a8623822cbdc193fd14706eb089071e3a5a20d60cc2eca59e4c5b2a61d29827a2f3362ee7c5f74f11d9ace200  disable-dlclose-test.patch
2842bcef71fb962cb49717fa9ba3e7318400c4a9175284a39ae3ff13a00260556794d0d63341430b2c64a0bca1ac76186e02c92e444a737a5dec1b6de27bbc60  dwarf-info.patch
5fa36157f7a76ca70d22af7bd96850db454d6add3a19cc3272962633c453087015755ca9df0de29cc40359e89279e470e91ff1a69d1453596a73291b36f39b23  dyld-elf-ppc32.patch
9abe376068801a09b2af01eef0cd319f48862b5ff7cce62af3cf4e7597a0898842125ae574577b545734ec1381f192b924b4f717a9c094f119e32ada81a2b9a2  hexagon.patch
de9791cc476817782b553c4edab696132da4ed2b9a3d80dbf1b85b7dc7a8c5e4caf14f2f26e33c27cd80a91b36709d3b24caea910c0a6315ffbb297cb748468d  macho32.patch
e5ddbc4b6c4928e79846dc3c022eb7928aaa8fed40515c78f5f03b8ab8264f34f1eb8aa8bfc0f436450932f4917e54ad261603032092ea271d9590f11a37cf1e  musl-ppc64-elfv2.patch
62de403e4cfd58d9f33f8b35ff31f66c906fd39c667fbcfa394d03f636fb10977c2f53c3a780d0c8e099f9a081951402757e12842d82a7cbd1aef4158f51d5c2  ppc-test.patch
89ef5fbab039b017a5652656adf17b680525f1c5dd0b1afc8034fe0b34a2ca196d87640f54cc826356cfcd88c6e5f10754b7e38f04ca49ede4f3864080b787bd  python3-test.patch
dca77af49ea181ab8d41630b8e7575b83aac22686a1119479abf8fe22d8f13032d69ef83a9635b9ad9b935cd3b51251d939e05967ad40db43abf69ac40e0eccd  roundeven.patch"

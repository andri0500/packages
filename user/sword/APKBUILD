# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=sword
pkgver=1.9.0
pkgrel=0
pkgdesc="Cross-platform library for accessing and searching OSIS books"
url="http://www.crosswire.org/sword/index.jsp"
arch="all"
license="GPL-2.0-only AND LGPL-2.1+ AND zlib"
depends=""
makedepends="curl-dev icu-dev xapian-core-dev zlib-dev"
subpackages="$pkgname-dev"
source="http://crosswire.org/ftpmirror/pub/sword/source/v${pkgver%.*}/sword-$pkgver.tar.gz"

build() {
	CPPFLAGS="$CPPFLAGS -DU_USING_ICU_NAMESPACE" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
	# These tests don't work on musl because argv[0] is a full path.
	for skiptest in basic mod2zmod osis2modcipher; do
		rm tests/testsuite/osis_${skiptest}.good;
	done
	make -C tests/testsuite run
}

package() {
	make DESTDIR="$pkgdir" install
	make DESTDIR="$pkgdir" install_config
}

sha512sums="9ed3fbb5024af1f93b1473bae0d95534d02a5b00b3c9d41a0f855cee8106dc4e330844080adbee7c3f74c0e5ce1480bf16c87c842421337a341f641bae11137f  sword-1.9.0.tar.gz"

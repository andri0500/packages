# Contributor: Sergey Lukin <sergej.lukin@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Pierre-Gilas MILLON <pgmillon@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libgit2
pkgver=1.6.4
pkgrel=0
pkgdesc="Pure C re-entrant library for custom Git applications"
url="https://libgit2.org/"
arch="all"
license="GPL-2.0-only"
depends=""
depends_dev="curl-dev libssh2-dev"
makedepends="$depends_dev cmake http-parser-dev openssl-dev python3 zlib-dev"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/$pkgname/$pkgname/archive/v$pkgver.tar.gz"

# secfixes:
#   0.27.3-r0:
#   - CVE-2018-10887
#   - CVE-2018-10888
#   - CVE-2018-11235
#   0.25.1-r0:
#   - CVE-2016-10128
#   - CVE-2016-10129
#   - CVE-2016-10130
#   0.24.3-r0:
#   - CVE-2016-8568
#   - CVE-2016-8569

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DPYTHON_EXECUTABLE=/usr/bin/python3 \
		.
	make
}

check() {
	# Don't run online tests by default.
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E 'online'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="fd73df91710f19b0d6c3765c37c7f529233196da91cf4d58028a8d3840244f11df44abafabd74a8ed1cbe4826d1afd6ff9f01316d183ace0924c65e7cf0eb8d5  libgit2-1.6.4.tar.gz"

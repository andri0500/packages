# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=systemsettings
pkgver=5.24.5
pkgrel=0
pkgdesc="KDE system settings configuration utility"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0+"
depends="kirigami2"
makedepends="qt5-qtbase-dev cmake extra-cmake-modules kactivities-stats-dev
	kactivities-dev kauth-dev kcmutils-dev kcompletion-dev kconfig-dev
	kconfigwidgets-dev kcoreaddons-dev kdbusaddons-dev khtml-dev ki18n-dev
	kiconthemes-dev kitemmodels-dev kio-dev kirigami2-dev krunner-dev
	kwindowsystem-dev kxmlgui-dev libkworkspace-dev kwidgetsaddons-dev
	kitemviews-dev kservice-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/systemsettings-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="7dd2aa3d2b0ad821569eaa6e17d064a2216d35a525baf82621a1fb31867a340372ec0349488cc6bf344156f9d9052b589be5d01ed82a1c18fcedaa64fb4986b1  systemsettings-5.24.5.tar.xz"

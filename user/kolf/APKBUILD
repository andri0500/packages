# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kolf
pkgver=22.04.2
pkgrel=0
pkgdesc="2D miniature golf game from KDE"
url="https://www.kde.org/applications/games/kolf/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kcompletion-dev
	kconfig-dev kconfigwidgets-dev kcoreaddons-dev kcrash-dev
	kdbusaddons-dev kdoctools-dev ki18n-dev kio-dev kwidgetsaddons-dev
	kxmlgui-dev libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kolf-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="057a47ee1553e68327e7f97170e9a4de31acd90d04f19b3aaae382bac870fd307d0bc14f3eb5db548615ee72decb25902e4b50ca012191c5771aad315c72b52f  kolf-22.04.2.tar.xz"

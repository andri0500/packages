# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=qt5-qtwebkit
_realname=qtwebkit
pkgver=5.212.0_git20200924
_ts=1600955993
pkgrel=1
pkgdesc="Open source Web browser engine"
url="https://github.com/qtwebkit/qtwebkit/wiki"
arch="all"
options="!check"  # Tests fail (surprise), require X11.
license="LGPL-2.1+ AND BSD-3-Clause AND Others"
depends="gst-plugins-base"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev ninja sqlite-dev icu-dev ruby perl bison flex gperf
	libxml2-dev libxslt-dev libjpeg-turbo-dev libpng-dev zlib-dev glib-dev
	gstreamer-dev fontconfig-dev qt5-qtsensors-dev qt5-qtpositioning-dev
	qt5-qtdeclarative-dev qt5-qtwebchannel-dev libxcomposite-dev cmake
	libxrender-dev gst-plugins-base-dev hyphen-dev libexecinfo-dev
	ruby-dev glib-dev libgcrypt-dev libtasn1-dev"
subpackages="$pkgname-dev"
#source="https://github.com/qtwebkit/qtwebkit/releases/download/$_realname-$_realver/$_realname-$_realver.tar.xz
source="https://download.qt.io/snapshots/ci/qtwebkit/${pkgver%.*}/$_ts/src/submodules/$_realname-opensource-src-${pkgver%.*}.tar.xz
	armv6.patch
	glib-compat.patch
	icu-68.patch
	jsc-musl.patch
	ppc-llint.patch
	ppc-ucontext.patch
	ppc64-llint.patch
	"
builddir="$srcdir"/$_realname-opensource-src-${pkgver%.*}

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi

	case "${CARCH}" in
		armv7|pmmx|ppc)
			# 32-bit memory ceiling (#922)
			BUILD_TYPE="MinSizeRel";
			LDFLAGS="${LDFLAGS} -Wl,--strip-debug";
			;;
		*)
			BUILD_TYPE="RelWithDebugInfo";
			;;
	esac

	# We can enable the JIT when it is stable on all Tier 1 platforms:
	# pmmx (ensure no SSE)
	# ppc
	# ppc64
	#
	# DONE:
	# aarch64
	# armv7
	# x86_64
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=$BUILD_TYPE \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DENABLE_JIT=OFF \
		-DENABLE_PRINT_SUPPORT=ON \
		-DENABLE_QT_WEBCHANNEL=ON \
		-DENABLE_SAMPLING_PROFILER=OFF \
		-DPORT=Qt \
		-DUSE_SYSTEM_MALLOC=ON \
		-DUSE_WOFF2=OFF \
		-DENABLE_MEDIA_SOURCE=ON \
		-DENABLE_VIDEO=ON \
		-DENABLE_WEB_AUDIO=ON \
		-DUSE_GSTREAMER=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="e2983ac291654ac75a8cdaaa1ca16810ae047e8d78c8910c415ab76e6efa532b75b119be8f053f00214ee5526099a75930becfbd23e8f7a4c08be32e5f41a1d7  qtwebkit-opensource-src-5.212.tar.xz
ca77802de1b945c05b6a88d5cc0cbb093a751ef16626d221c8b2ca5cf841f5d42fffe62087df1ce0964f8ce72148c0109ff85e5cef3390125c133b310031028e  armv6.patch
9f42f3d64657fb9487d9299ad5931381f9aa91a746a5986dc361def5e9a6e02933054a66132fa99057460ad6de0c19e9b51e07594451cc4f38b19012a69d205c  glib-compat.patch
5f0ca182c68c55a6f221e96bf2221784c7126006ea8db3e9eee2b66dbdda18d7d0f9830e8345ac2b0bc6f557af5d2a54a321b06be4a2c845dd1003282b053443  icu-68.patch
9e3638d4d5c6e56510525931b278c8d6e28134917c300837b4eccf1b9298af1e274668318af82091137e99b83da0f78904084b7ee9dd8422b347a0f35e765c31  jsc-musl.patch
4a3a15f8b1e63cade07c589be45afd794b45b34ee98e4d2d3fc2f52662c26c518d400b44c9314e41113cad847b9efd544d2a4c02425c9892ca9729e218ae9306  ppc-llint.patch
48f81c6a2c0f4e9b215dada4c0bebdafc66feb75178a7b1ca661f2bbcddd6b573e7db4dd179f6e4b6345c7ebcf17ce1c6647cc6ce39dbac8ba68f8863a98bdc0  ppc-ucontext.patch
db98d710815eb68f1fb025098b04c34b33518750c193e702383ca879e145c52ba4786fa4721f384f01f90231c32be9478d507c1000eb761621751edcd071b966  ppc64-llint.patch"

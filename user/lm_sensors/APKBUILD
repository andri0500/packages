# Maintainer: 
pkgname=lm_sensors
_pkgname=lm-sensors
pkgver=3.6.0
_pkgver=$(printf '%s' "$pkgver" | tr . -)
pkgrel=0
pkgdesc="Collection of user space tools for general SMBus access and hardware monitoring."
url="https://hwmon.wiki.kernel.org/lm_sensors"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0-only AND LGPL-2.1+ AND GPL-2.0+"
depends="bash sysfsutils"
makedepends="bison flex perl rrdtool-dev cmd:which"
subpackages="$pkgname-dev $pkgname-doc $pkgname-detect $pkgname-sensord
	$pkgname-sensord-openrc:sensord_openrc:noarch"
source="$pkgname-$pkgver.tar.gz::https://github.com/lm-sensors/lm-sensors/archive/V$_pkgver.tar.gz
	use-module-load.d-dir.patch
	fancontrol.initd
	sensord.confd
	sensord.initd
	"
builddir="$srcdir/$_pkgname-$_pkgver"

prepare() {
	default_prepare

	sed -i -e 's:^# \(PROG_EXTRA\):\1:' Makefile
	# Respect LDFLAGS
	sed -i -e 's/\$(LIBDIR)$/\$(LIBDIR) \$(LDFLAGS)/g' Makefile
	sed -i -e 's/\$(LIBSHSONAME) -o/$(LIBSHSONAME) \$(LDFLAGS) -o/g' \
		lib/Module.mk

	# do not check for libiconv in ldconfig cache
	sed -i -e 's/^LIBICONV.*/LIBICONV ?=/' prog/sensors/Module.mk
}

build() {
	export CFLAGS="$CFLAGS -fno-stack-protector"
	make PREFIX=/usr user
}

package() {
	make PROG_EXTRA:=sensord user_install \
		PREFIX=/usr \
		MANDIR=/usr/share/man \
		DESTDIR="$pkgdir"

	install -Dm755 "$srcdir"/fancontrol.initd "$pkgdir"/etc/init.d/fancontrol
}

detect() {
	depends="perl"
	pkgdesc="Detection/migration scripts for lm_sensors"
	mkdir -p "$subpkgdir"/usr/bin "$subpkgdir"/usr/sbin
	mv "$pkgdir"/usr/bin/sensors-conf-convert "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/sbin/sensors-detect "$subpkgdir"/usr/bin/
}

sensord() {
	pkgdesc="sensord daemon"
	mkdir -p "$subpkgdir"/usr/sbin
	mv "$pkgdir"/usr/sbin/sensord "$subpkgdir"/usr/sbin/sensord
}

sensord_openrc() {
	pkgdesc="sensord daemon (OpenRC init scripts)"
	install_if="sensord=$pkgver-r$pkgrel openrc"
	install -Dm755 "$srcdir"/sensord.initd "$subpkgdir"/etc/init.d/sensord
	install -Dm755 "$srcdir"/sensord.confd "$subpkgdir"/etc/conf.d/sensord
}

sha512sums="4e80361913aff5403f1f0737fd4f42cffe43cc170ef48fff3914c9952f71990739d723f7b0b8120d9a01bcbbc829e964cfbd0a5cf18508af8f8dc825b49860bf  lm_sensors-3.6.0.tar.gz
794cf2aaa2a9e809c6b67f4c888a89064bba3e5b9333a9f0101a92372c25012e506fa48e86523f57cf30e5c2a808bc38058fd8640c870ea6b48faab44794cfbb  use-module-load.d-dir.patch
04756c3844033dc7897e1348181140a43f8470c1bb863f1524b21bbe6be2f13fbf17ac3a68270c96a70d8c148124fea569d1ef75619bbe383e15ec705ea18b21  fancontrol.initd
a77d81ab7ded085ba19e4c637e93268f889ccb8ce9e008a210ae135cb6e2140be07e5d455cf7fcc1084fd57cfbfb3f2bb37207123aebe9566f78b5183806fd7d  sensord.confd
9a19874c158e82ab076ed5fb96a40d4bfb4957bfd5a2ce66aa207c06e577bc1b048336c0046a9f856f6d00dc10e68a0dc9726f6e726a8f7bfd50c4043ee1e26a  sensord.initd"
